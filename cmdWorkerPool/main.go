package main

import (
	"flag"
	"fmt"
	"golang.org/x/net/proxy"
	"net/http"
	"net/url"
	"os"
	"time"

	"log"
	"myfloridalicense/handlers"
)

var (
	LType                    string
	state                    string
	socksStr                 string
	NumSearchFormDataWorkers int
	NumSearchResultWorkers   int
)

func init() {
	//flag.StringVar(&filename, "file", "", "Filename csv file. By default if '-c' empty filename is 'out.csv' otherwise '[categoryname].csv'")
	flag.StringVar(&LType, "lt", "", "License type")
	flag.StringVar(&state, "st", "Florida", "State for iterate")
	flag.StringVar(&socksStr, "socks", "socks5://127.0.0.1:9050", "Socks string")
	flag.IntVar(&NumSearchFormDataWorkers, "nSFD", 30, "Number SearchFormData Workers")
	flag.IntVar(&NumSearchResultWorkers, "nSR", 50, "Number SearchResult Workers")

	//flag.DurationVar(&requestDelay, "spr", 250*time.Millisecond, "Delay between new queries")
	//flag.BoolVar(&debug, "debug", true, "Delay between new queries , default 75ms")

	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, "Usage: tool [options] params>\n\n")
		flag.PrintDefaults()
		os.Exit(0)
	}
}

func fatalf(fmtStr string, args interface{}) {
	fmt.Fprintf(os.Stderr, fmtStr, args)
	os.Exit(-1)
}

func main() {
	flag.Parse()

	tbProxyURL, err := url.Parse(socksStr)
	if err != nil {
		fatalf("Failed to parse proxy URL: %v\n", err)
	}

	// Get a proxy Dialer that will create the connection on our
	// behalf via the SOCKS5 proxy.  Specify the authentication
	// and re-create the dialer/transport/client if tor's
	// IsolateSOCKSAuth is needed.
	tbDialer, err := proxy.FromURL(tbProxyURL, proxy.Direct)
	if err != nil {
		fatalf("Failed to obtain proxy dialer: %v\n", err)
	}

	// Make a http.Transport that uses the proxy dialer, and a
	// http.Client that uses the transport.
	tbTransport := &http.Transport{
		Dial:                tbDialer.Dial,
		TLSHandshakeTimeout: time.Second * 20,
	}
	client := &http.Client{Transport: tbTransport, Timeout: time.Minute * 2}
	http.DefaultClient = client

	if LType == "" || state == "" {
		fmt.Fprintf(os.Stderr, "License type must be not empty \n\n")
		flag.Usage()
		os.Exit(1)
	}

	workersPool, err := handlers.NewWorkerPool(handlers.WorkerPoolParams{NumSearchResultWorkers: NumSearchResultWorkers,
		NumSearchFormDataWorkers: NumSearchFormDataWorkers,
		GetClient:                getSocksClient,
		Filename:                 LType + ".csv",
		LicenseType:              LType,
		State:                    state,
	})
	check(err)
	err = workersPool.Run()
	check(err)
}

func check(e error) {
	if e != nil {
		//panic(e)
		log.Fatal(e.Error())
	}
}

func getSocksClient() *http.Client {
	return http.DefaultClient

}
