package handlers

import (
	"context"
	"fmt"
	"myfloridalicense"
	"net/http"
	"strings"
	"sync"
	"time"
)

type WorkerPoolParams struct {
	NumSearchFormDataWorkers int
	NumSearchResultWorkers   int
	GetClient                func() *http.Client
	Filename                 string
	LicenseType              string
	State                    string
}

type WorkersPool struct {
	NumSearchFormDataWorkers int
	NumSearchResultWorkers   int
	LicenseType              string
	State                    string

	wgWorkerSearchFormData *sync.WaitGroup
	wgWorkerSearchResult   *sync.WaitGroup
	chSearchFormData       chan myfloridalicense.SearchLicenseForm
	chSearchResult         chan reqParams
	csvWriter              *CsvWriterAndWG

	getClient func() *http.Client
	ctx       context.Context

	skipAll     bool
	skipRWmutex sync.RWMutex

	//countSearchResultDone uint
}

func NewWorkerPool(params WorkerPoolParams) (*WorkersPool, error) {
	workerPool := new(WorkersPool)

	workerPool.getClient = params.GetClient
	workerPool.wgWorkerSearchFormData = &sync.WaitGroup{}
	workerPool.wgWorkerSearchResult = &sync.WaitGroup{}
	workerPool.chSearchFormData = make(chan myfloridalicense.SearchLicenseForm)
	workerPool.chSearchResult = make(chan reqParams, 20)
	workerPool.LicenseType = params.LicenseType
	workerPool.State = params.State
	workerPool.NumSearchFormDataWorkers = params.NumSearchFormDataWorkers
	workerPool.NumSearchResultWorkers = params.NumSearchResultWorkers

	csv, err := NewCsvWriter(params.Filename)
	if err != nil {
		return nil, err
	}
	workerPool.csvWriter = csv

	return workerPool, nil
}

func (wp *WorkersPool) Run() error {
	//run workers

	for i := 0; i < wp.NumSearchFormDataWorkers; i++ {
		wp.wgWorkerSearchFormData.Add(1)
		go wp.WorkerSearchFormData(i)
	}
	fmt.Println("Workers SearchFormData started")
	for i := 0; i < wp.NumSearchFormDataWorkers; i++ {
		wp.wgWorkerSearchResult.Add(1)
		go wp.WorkerSearchResult(i)
	}
	fmt.Println("Workers SearchResult started")

	//defer close(wp.chSearchFormData)
	err := wp.WorkerGetFormData()

	close(wp.chSearchFormData)
	fmt.Println("Wait workers SearchResult")
	wp.wgWorkerSearchFormData.Wait()
	close(wp.chSearchResult)
	fmt.Println("Wait workers SearchResult")
	wp.wgWorkerSearchResult.Wait()

	fmt.Println("Flush and save data")
	wp.csvWriter.FlushAndClose()

	return err
}
func (wp *WorkersPool) SetSkipAll() {
	wp.skipRWmutex.Lock()
	defer wp.skipRWmutex.Unlock()
	wp.skipAll = true
}
func (wp *WorkersPool) isSkipAll() bool {
	wp.skipRWmutex.RLock()
	defer wp.skipRWmutex.RUnlock()
	return wp.skipAll
}

func (wp *WorkersPool) WorkerGetFormData() error {
	//get client
	client := wp.getClient()

	LicenseCategories, err := GetLicenseCategories(client)
	if err != nil {
		return fmt.Errorf("Error get License Categories: ", err.Error())
	}
	for _, FV := range LicenseCategories {
		var found bool
		SF, err := GetLicenseTypeCountryState(client, FV)
		if err != nil {
			return fmt.Errorf("Error get License Types: ", err.Error())
		}

		for _, LicenseType := range SF.LicenseType {
			if found || wp.isSkipAll() {
				goto OUT
			}
			//fmt.Println(LicenseType.Name)
			if wp.LicenseType != "" && wp.LicenseType != LicenseType.Name {
				fmt.Printf("Search %s in category %s\n ", wp.LicenseType, LicenseType.Name) //category != LicenseType.Name
				continue
			}
			if wp.LicenseType == LicenseType.Name {
				fmt.Println("Found")
				found = true
			}

			for _, County := range SF.County {
				for _, State := range SF.State {
					if wp.isSkipAll() {
						goto OUT
					}

					if strings.TrimSpace(State.Name) != wp.State {
						continue
					}

					params := myfloridalicense.SearchLicenseForm{LicenseCategory: SF.LicenseCategory,
						LicenseType: LicenseType,
						County:      County,
						State:       State,
					}

					fmt.Println("send data to WorkerSearchFormData")
					wp.chSearchFormData <- params
				}
			}

		}
	}
OUT:
	return nil
}

func (wp *WorkersPool) WorkerSearchFormData(id int) {
	defer wp.wgWorkerSearchFormData.Done()
	defer fmt.Println("close SearchFormDataWorker ", id)
	fmt.Println("Start SearchFormDataWorker ", id)

	var client *http.Client
	client = wp.getClient()
	for formData := range wp.chSearchFormData {
		fmt.Println("WorkerSearchFormData id: ", id, " got data from ch")

		//	make req
		again := true
		var sr SearchResult
		//var err error

		for again {
			fmt.Println("WorkerSearchFormData id: ", id, " start req")
			sr1, err := GetSearchResults(client, formData)
			//	handle err
			//	send to chSearchResult <-
			if err != nil {
				fmt.Println("Error get search results: ", err.Error())
				if wp.isSkipAll() {
					return
				}
				client = wp.getClient()
			} else {
				//WorkerSearchFormData
				fmt.Println("WorkerSearchFormData id: ", id, " got data from req, len ", len(sr.ResultArray))
				again = false
				sr = sr1
			}
		}

		for _, serchResult := range sr.ResultArray {
			if wp.isSkipAll() {
				return
			}
			fmt.Println("WorkerSearchFormData send data to chSearchResult")
			wp.chSearchResult <- reqParams{formData, serchResult}
		}

		NextPage := sr.NextPage

		var page int = 2
		for !wp.isSkipAll() && NextPage {
			again := true
			for again {
				//fmt.Printf("WorkerSearchFormData id: handle page %d \n", id, i, sr.InputsVal)
				sr1, err := GetSearchResultsPage(client, sr)
				if err != nil {
					fmt.Println("Error get search results page", page, " : ", err.Error())
					if wp.isSkipAll() {
						return
					}
					client = wp.getClient()
				} else {
					again = false
					sr = sr1
				}
			}

			NextPage = sr.NextPage
			if NextPage && sr.InputsVal.Get("hCurrPage") == sr.InputsVal.Get("hTotalPages") {
				NextPage = false
			}

			fmt.Printf("GetSearchResultsPage id: %d; hCurrPage %s hTotalPages:%s\n", id, sr.InputsVal.Get("hCurrPage"), sr.InputsVal.Get("hTotalPages"))
			page++

			for _, serchResult := range sr.ResultArray {
				if wp.isSkipAll() {
					break
				}
				wp.chSearchResult <- reqParams{formData, serchResult}
			}
		}
	}
}

func (wp *WorkersPool) WorkerSearchResult(num int) {
	defer wp.wgWorkerSearchResult.Done()
	defer fmt.Println("close SearchFormDataWorker ", num)

	fmt.Println("Start SearchResult ", num)
	var client *http.Client
	client = wp.getClient()
	//fmt.Println("wait data SearchResult ", num)
	for params := range wp.chSearchResult {
		if !wp.csvWriter.IsUniqueURL(params.SearchResult) {
			continue
		}
		fmt.Println("Worker SearchResult id: ", num, " started scrape new item")

		again := true
		var details myfloridalicense.LicenseeDetails
		var err error

		for again {
			details, err = GetLicenseDetails(client, params.SearchResult)
			if err != nil {
				fmt.Println("Error get search result: ", err.Error(), "\n will sleep 240 seconds to next handle:", "https://www.myfloridalicense.com/"+params.Href)
				if wp.isSkipAll() {
					return
				}
				client = wp.getClient()
				time.Sleep(time.Second * 120)
			} else {
				again = false
			}
		}
		fmt.Println("SearchResult ", num, " put data to csv")
		wp.csvWriter.Write(details, params.SearchLicenseForm, params.SearchResult)
		//	make req
		//	handle err
		//send to csv
	}

}

type reqParams struct {
	myfloridalicense.SearchLicenseForm
	myfloridalicense.SearchResult
}
