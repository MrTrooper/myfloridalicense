package handlers

import (
	"fmt"
	"github.com/PuerkitoBio/goquery"
	"myfloridalicense"
	"myfloridalicense/parser"
	"net/http"
	"net/url"
)

func GetLicenseCategories(client *http.Client) ([]myfloridalicense.FormValue, error) {
	doc, err := goquery.NewDocument("https://www.myfloridalicense.com/wl11.asp?mode=1&SID=&brd=&typ=N")
	if err != nil {
		return nil, err
	}
	return parser.ParseLicenseCategory(doc), nil
}

func GetLicenseTypeCountryState(client *http.Client, LicenseCategory myfloridalicense.FormValue) (searchForms myfloridalicense.SearchFormsForLicenseCategory, err error) {
	data := GetFormDefaultData()
	data.Set("Board", LicenseCategory.ID)
	resp, err := client.PostForm("https://www.myfloridalicense.com/wl11.asp?mode=1&SID=&brd=&typ=N", data)
	if err != nil {
		return searchForms, err
	}
	//defer resp.Body.Close()
	doc, err := goquery.NewDocumentFromResponse(resp)

	searchForms.LicenseCategory = LicenseCategory
	searchForms.LicenseType = parser.ParseLicenseType(doc)
	searchForms.County = parser.ParseCountry(doc)
	searchForms.State = parser.ParseState(doc)
	return searchForms, err
}

type SearchResult struct {
	ResultArray []myfloridalicense.SearchResult
	NextPage    bool
	InputsVal   url.Values
}

func GetSearchResults(client *http.Client, formData myfloridalicense.SearchLicenseForm) (SearchResult, error) {
	var res = SearchResult{InputsVal: url.Values{}}

	fd := GetFormData(formData)
	fd.Set("Search1", "Search")
	resp, err := client.PostForm("https://www.myfloridalicense.com/wl11.asp?mode=2&SID=&brd=&typ=N", fd)
	if err != nil {
		return res, err
	}

	doc, err := goquery.NewDocumentFromResponse(resp)
	if err != nil {
		return res, err
	}

	ParseSearchResultsArr, NextPage := parser.ParseSearchResults(doc)
	res.ResultArray = ParseSearchResultsArr
	res.NextPage = NextPage
	if NextPage {
		res.InputsVal = parser.ParseHiddenInputs(doc)
	}
	return res, nil
}

func GetSearchResultsPage(client *http.Client, lastResult SearchResult) (SearchResult, error) {
	var res = SearchResult{InputsVal: url.Values{}}

	values := lastResult.InputsVal

	values.Set("hPageAction", "4")
	values.Set("SearchForward", "Search")
	resp, err := client.PostForm("https://www.myfloridalicense.com/wl11.asp?mode=3&SID=&brd=&typ=N", values)
	if err != nil {
		return res, err
	}

	doc, err := goquery.NewDocumentFromResponse(resp)
	if err != nil {
		return res, err
	}
	arr, NextPage := parser.ParseSearchResults(doc)
	res.ResultArray = arr
	res.NextPage = NextPage

	if NextPage {
		res.InputsVal = parser.ParseHiddenInputs(doc)
	}
	return res, nil
}

func GetLicenseDetails(client *http.Client, ResultsUrl myfloridalicense.SearchResult) (ld myfloridalicense.LicenseeDetails, err error) {
	resp, err := client.Get("https://www.myfloridalicense.com/" + ResultsUrl.Href)
	if err != nil {
		return ld, err
	}
	doc, err := goquery.NewDocumentFromResponse(resp)
	if err != nil {
		//panic(err)
		return ld, err
	}
	if parser.ParseCannotBeProcessed(doc) {
		return ld, fmt.Errorf("Sorry, but your request cannot be processed at this time. Please try again later.")
	}
	return parser.ParseLicenseDetails(doc), nil
}

func GetFormDefaultData() url.Values {
	data := url.Values{}
	//data.Set("hSID", "")
	//data.Set("hSearchType", "")
	//data.Set("hLastName", "")
	//data.Set("hFirstName", "")
	//data.Set("hMiddleName", "")
	//data.Set("hOrgName", "")
	//data.Set("hSearchOpt", "")
	//data.Set("hSearchOpt2", "")
	//data.Set("hSearchAltName", "")
	//data.Set("hSearchPartName", "")
	//data.Set("hSearchFuzzy", "")
	//data.Set("hDivision", "ALL")
	//data.Set("hBoard", "")
	//data.Set("hLicenseType", "")
	//data.Set("hSpecQual", "")
	//data.Set("hAddrType", "")
	//data.Set("hCity", "")
	//data.Set("hCounty", "")
	//data.Set("hState", "")
	//data.Set("hLicNbr", "")
	//data.Set("hAction", "")
	//data.Set("hCurrPage", "")
	//data.Set("hTotalPages", "")
	//data.Set("hTotalRecords", "")
	//data.Set("hPageAction", "")
	//data.Set("hDDChange", "Y")
	//data.Set("hBoardType", "")
	//data.Set("hLicTyp", "")
	//data.Set("hSearchHistoric", "")
	//data.Set("hRecsPerPage", "10")
	data.Set("Board", "400")
	data.Set("City", "")
	data.Set("County", "")
	data.Set("State", "")
	data.Set("RecsPerPage", "50")
	data.Set("hSID", "")
	data.Set("hSID", "")
	return data
}

func GetFormData(formData myfloridalicense.SearchLicenseForm) url.Values {
	values := GetFormDefaultData()
	values.Set("Board", formData.LicenseCategory.ID)
	values.Set("LicenseType", formData.LicenseType.ID)
	values.Set("County", formData.County.ID)
	values.Set("State", formData.State.ID)
	return values
}
