package handlers

import (
	"encoding/csv"
	"fmt"
	"myfloridalicense"
	"os"
	"sync"
)

type CsvWriterAndWG struct {
	//sync.WaitGroup
	file       *os.File
	csv        *csv.Writer
	n          int
	count      int
	mx         *sync.Mutex
	unicURLs   map[string]bool
	mxUnicUrls *sync.Mutex
}

func NewCsvWriter(filename string) (*CsvWriterAndWG, error) {
	var csvWr = &CsvWriterAndWG{}
	file, err := os.Create(filename)
	if err != nil {
		return nil, err
	}
	csvWr.file = file
	csvWriter := csv.NewWriter(file)
	csvWr.csv = csvWriter
	csvWr.mx = &sync.Mutex{}
	csvWr.mxUnicUrls = &sync.Mutex{}
	csvWr.unicURLs = make(map[string]bool)

	csvWr.csv.Write([]string{"name", "address", "license number", "status", "expires", "License Category", "License Type", "County", "State", "url"})
	return csvWr, nil
}

func (wr *CsvWriterAndWG) Write(detals myfloridalicense.LicenseeDetails, formData myfloridalicense.SearchLicenseForm, href myfloridalicense.SearchResult) {
	wr.mx.Lock()
	defer wr.mx.Unlock()
	url := "https://www.myfloridalicense.com/" + href.Href
	wr.csv.Write([]string{detals.Name, detals.MainAddress, detals.LicenseNumber, detals.Status, detals.Expires, formData.LicenseCategory.Name, formData.LicenseType.Name, formData.County.Name, formData.State.Name, url})
	if wr.n > 2000 {
		wr.n = 0
		wr.csv.Flush()
	}
	wr.n++
	wr.count++
	if wr.count%50 == 0 {
		fmt.Printf("Writed %d items\n", wr.count)
	}
}

func (wr *CsvWriterAndWG) Flush() {
	wr.mx.Lock()
	defer wr.mx.Unlock()
	wr.csv.Flush()
	wr.n = 0
}

func (wr *CsvWriterAndWG) FlushAndClose() {
	fmt.Println("Flush And Close")
	wr.csv.Flush()
	wr.file.Close()
}

//func (wr *CsvWriterAndWG) WaitFlushAndClose() {
//	wr.Wait()
//	wr.mx.Lock()
//	defer wr.mx.Unlock()
//	wr.FlushAndClose()
//}

func (wr *CsvWriterAndWG) IsUniqueURL(SRurl myfloridalicense.SearchResult) bool {
	wr.mxUnicUrls.Lock()
	defer wr.mxUnicUrls.Unlock()
	if wr.unicURLs[SRurl.Href] {
		return false
	}
	wr.unicURLs[SRurl.Href] = true
	return true
}
