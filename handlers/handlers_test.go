package handlers

import (
	"fmt"
	"myfloridalicense"
	"net/http"
	"testing"
)

func TestGetData(t *testing.T) {
	var SearchLicenseForm myfloridalicense.SearchLicenseForm

	var slf myfloridalicense.SearchFormsForLicenseCategory
	cat, err := GetLicenseCategories(http.DefaultClient)
	if err != nil {
		t.Fatal(err.Error())
	}
	for _, lc := range cat {
		if lc.Name == "Cosmetology" {
			slf, err = GetLicenseTypeCountryState(http.DefaultClient, lc)
			if err != nil {
				t.Fatal(err.Error())
			}
		}
	}
	if slf.LicenseCategory.Name == "" {
		t.Error("LicenseCategory is empty")
	}

	SearchLicenseForm.LicenseCategory = slf.LicenseCategory

	for _, q := range slf.LicenseType {
		if q.Name == "Cosmetologist" {
			SearchLicenseForm.LicenseType = q
		}
	}

	for _, q := range slf.County {
		if q.Name == "Bay" {
			SearchLicenseForm.County = q
		}
	}
	for _, q := range slf.State {
		if q.Name == "Florida" {
			SearchLicenseForm.State = q
		}
	}

	//fmt.Printf("%#n", SearchLicenseForm)

	sr, err := GetSearchResults(http.DefaultClient, SearchLicenseForm)
	if err != nil {
		t.Fatal(err.Error())
	}
	NextPage := sr.NextPage
	var i int
	for NextPage {
		fmt.Println(i)
		sr, err = GetSearchResultsPage(http.DefaultClient, sr)
		if err != nil {
			t.Fatal(err.Error())
		}
		NextPage = sr.NextPage
		fmt.Println("GetSearchResultsPage len:", len(sr.ResultArray))
		i++

	}
	if i < 4 {
		t.Fail()
	}

}

//func TestGetSearchResultsPage(t *testing.T) {
//	formData := myfloridalicense.SearchLicenseForm{LicenseCategory: myfloridalicense.FormValue{ID: "05", Name: "Cosmetology"}, LicenseType: myfloridalicense.FormValue{ID: "0501", Name: "Cosmetologist"}, County: myfloridalicense.FormValue{ID: "13", Name: "Bay"}, State: myfloridalicense.FormValue{ID: "FL", Name: "Florida"}}
//	arr, nextPage, _ := GetSearchResultsPage(formData)
//	fmt.Printf("len %d, nextPage %b", len(arr), nextPage)
//
//}
