package myfloridalicense

type FormValue struct {
	ID   string
	Name string
}

type SearchLicenseForm struct {
	LicenseCategory FormValue
	LicenseType     FormValue
	County          FormValue
	State           FormValue
}

type SearchFormsForLicenseCategory struct {
	LicenseCategory FormValue
	LicenseType     []FormValue
	County          []FormValue
	State           []FormValue
}

type SearchResult struct {
	Name string
	Href string
}

type LicenseeDetails struct {
	Name          string
	MainAddress   string
	LicenseNumber string
	Status        string
	Expires       string
}
