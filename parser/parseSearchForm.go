package parser

import (
	"github.com/PuerkitoBio/goquery"
	"myfloridalicense"
)

func ParseLicenseCategory(doc *goquery.Document) (formSelect []myfloridalicense.FormValue) {
	//formSelect = make(map[string]string)
	doc.Find(`form [name="Board"] option`).Each(func(index int, item *goquery.Selection) {
		id, ok := item.Attr("value")
		if ok && item.Text() != "" {
			formSelect = append(formSelect, myfloridalicense.FormValue{ID: id, Name: item.Text()})
		}
	})
	return formSelect
}

func ParseLicenseType(doc *goquery.Document) (formSelect []myfloridalicense.FormValue) {
	//formSelect = make(map[string]string)

	doc.Find(`form [name="LicenseType"] option`).Each(func(index int, item *goquery.Selection) {
		id, ok := item.Attr("value")
		if ok && item.Text() != "" {
			//formSelect[item.Text()] = id
			formSelect = append(formSelect, myfloridalicense.FormValue{ID: id, Name: item.Text()})
		}
	})
	return formSelect
}

func ParseCountry(doc *goquery.Document) (formSelect []myfloridalicense.FormValue) {
	//formSelect = make(map[string]string)
	doc.Find(`form [name="County"] option`).Each(func(index int, item *goquery.Selection) {
		id, ok := item.Attr("value")
		if ok && item.Text() != "" {
			//formSelect[item.Text()] = id
			formSelect = append(formSelect, myfloridalicense.FormValue{ID: id, Name: item.Text()})
		}
	})
	return formSelect
}

func ParseState(doc *goquery.Document) (formSelect []myfloridalicense.FormValue) {
	//formSelect = make(map[string]string)
	doc.Find(`form [name="State"] option`).Each(func(index int, item *goquery.Selection) {
		id, ok := item.Attr("value")
		if ok && item.Text() != "" {
			//formSelect[item.Text()] = id
			formSelect = append(formSelect, myfloridalicense.FormValue{ID: id, Name: item.Text()})
		}
	})
	return formSelect
}
