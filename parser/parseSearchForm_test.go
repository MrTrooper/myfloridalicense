package parser

import (
	"fmt"
	"github.com/PuerkitoBio/goquery"
	"os"
	"testing"
)

func OpenQueryFile(fileName string) *goquery.Document {
	file, err := os.Open(fileName)
	if err != nil {
		panic(err)
	}
	doc, err := goquery.NewDocumentFromReader(file)
	if err != nil {
		panic(err)
	}
	return doc
}

//
//var mapTestCategoryMap = map[string]string{"Pari-Mutuel Wagering - Slots": "101", "Building Code Administrators and Inspectors": "50", "Barbers": "03", "Community Association Managers and Firms": "38", "Landscape Architecture": "13", "Real Estate Appraisers": "64", "Architecture & Interior Design": "02", "Auctioneers": "48", "Boxing, Kick Boxing & Mixed Martial Arts": "600", "Hotels and Restaurants": "200", "Labor Organizations": "74", "Mold-Related Services": "07", "Pari-Mutuel Wagering": "100", "Yacht and Ships": "85", "Athlete Agents": "60", "Certified Public Accounting": "01", "Electrical Contractors": "08", "Engineers": "09", "Alcoholic Beverages & Tobacco": "400", "Mobile Homes": "810", "Real Estate": "25", "Employee Leasing Companies": "63", "CTMH Other Entities": "830", "Condominiums, Cooperatives, Timeshares, & Multi-Site Timeshares": "800", "Cosmetology": "05", "Farm Labor": "75", "Harbor Pilots": "23", "Land Sales": "820", "Veterinary Medicine": "26", "Asbestos Contractors and Consultants": "59", "Geologists": "53", "Homeowners' Associations": "840", "Talent Agencies": "49", "Drugs, Devices and Cosmetics": "33", "Elevator Safety": "210", "Home Inspectors": "04", "Construction Industry": "06"}
//
//func TestParseLicenseCategory(t *testing.T) {
//	categoryMap := ParseLicenseCategory(OpenQueryFile("./testfiles/out0.html"))
//	for name, id := range mapTestCategoryMap {
//		if categoryMap[name] != id {
//			t.Fail()
//		}
//	}
//}
//
//func TestParseLicenseType(t *testing.T) {
//	var TestLicenseTypeMap = map[string]string{"Bottle Club": "4014", "Brand Registrant": "4011", "Common Carriers": "4004", "Manufacturers/Distributors": "4005", "Retail Beverage": "4006", "Retail Tobacco Products Dealer": "4012", "Salesperson Wine & Spirits": "4013", "Brands": "4008", "Permits": "4003", "Pool Buying": "4007", "Quota Drawing Business Entry": "4088", "Quota Drawing Individual Entry": "4087", "Tobacco Wholesaler's, Distributors & Exp": "4001"}
//
//	LicenseTypeMap := ParseLicenseType(OpenQueryFile("./testfiles/out.html"))
//	for name, id := range TestLicenseTypeMap {
//		if LicenseTypeMap[name] != id {
//			t.Fail()
//		}
//	}
//}
//
//func TestParseCountry(t *testing.T) {
//	var TestCountryMap = map[string]string{"Alachua": "11", "Baker": "12", "Pasco": "61", "Santa Rosa": "67", "Sarasota": "68", "Union": "73", "Brevard": "15", "Levy": "48", "Okeechobee": "57", "Escambia": "27", "Manatee": "51", "Putnam": "64", "Bradford": "14", "Broward": "16", "Citrus": "19", "Highlands": "38", "Sumter": "70", "Indian River": "41", "Washington": "77", "Charlotte": "18", "Gadsden": "30", "Holmes": "40", "Columbia": "22", "Marion": "52", "Gulf": "33", "Duval": "26", "Gilchrist": "31", "Liberty": "49", "Taylor": "72", "Bay": "13", "Dade": "23", "DeSoto": "24", "Nassau": "55", "Orange": "58", "Out of State": "79", "Volusia": "74", "Hernando": "37", "Jackson": "42", "Monroe": "54", "Okaloosa": "56", "Osceola": "59", "Palm Beach": "60", "Wakulla": "75", "": "", "Collier": "21", "Lafayett": "44", "Leon": "47", "Martin": "53", "Unknown": "78", "Calhoun": "17", "Dixie": "25", "Hamilton": "34", "Lake": "45", "Suwannee": "71", "Glades": "32", "Hendry": "36", "Jefferson": "43", "Polk": "63", "Franklin": "29", "Hillsborough": "39", "Pinellas": "62", "Lee": "46", "St. Johns": "65", "Walton": "76", "Clay": "20", "Foreign": "80", "Hardee": "35", "Madison": "50", "Flagler": "28", "Seminole": "69", "St. Lucie": "66"}
//
//	CountryMap := ParseCountry(OpenQueryFile("./testfiles/out.html"))
//	for name, id := range TestCountryMap {
//		if CountryMap[name] != id {
//			t.Fail()
//		}
//	}
//}
//
func TestParseState(t *testing.T) {
	var TestStateMap = map[string]string{"Michigan": "MI", "Rhode Island": "RI", "Armed Force-Americas": "AA", "Indiana": "IN", "South Dakota": "SD", "Canal Zone": "CZ", "District of Columbia": "DC", "Connecticut": "CT", "Illinois": "IL", "New York": "NY", "Tennessee": "TN", "Washington": "WA", "Wyoming": "WY", "California": "CA", "Colorado": "CO", "Marshall Islands": "MH", "Nebraska": "NE", "North Carolina": "NC", "Oregon": "OR", "Florida": "FL", "Iowa": "IA", "Virginia": "VA", "Kentucky": "KY", "Utah": "UT", "Georgia": "GA", "Missouri": "MO", "Hawaii": "HI", "Maine": "ME", "Montana": "MT", "Palau": "PW", "Armed Force-Pacific": "AP", "Guam": "GU", "Mississippi": "MS", "Wisconsin": "WI", "Minnesota": "MN", "Ohio": "OH", "Unknown": "99", "Foreign PO Americas": "FA", "Kansas": "KS", "Foreign PO Europe": "FE", "Massachusetts": "MA", "Arkansas": "AR", "Armed Force-Europe": "AE", "New Jersey": "NJ", "New Mexico": "NM", "Foreign PO Pacific": "FP", "Nevada": "NV", "Virgin Island": "VI", "Idaho": "ID", "Puerto Rico": "PR", "Maryland": "MD", "Oklahoma": "OK", "South Carolina": "SC", "Alaska": "AK", "Louisiana": "LA", "Delaware": "DE", "Vermont": "VT", "Alabama": "AL", "Arizona": "AZ", "New Hampshire": "NH", "Texas": "TX", "": "", "Federated States": "FM", "Pennsylvania": "PA", "West Virginia": "WV", "American Samoa": "AS", "North Dakota": "ND"}

	StateMap := ParseState(OpenQueryFile("./testfiles/out.html"))
	for _, state := range StateMap {
		//fmt.Println(1)
		//fmt.Println(state.Name)
		//fmt.Println(state.ID)

		if TestStateMap[state.Name] != state.ID {
			t.Fail()
		}
	}
}

//
//func TestPostForm(t *testing.T) {
//	data := url.Values{}
//	data.Set("hSID", "")
//	data.Set("hSearchType", "")
//	data.Set("hLastName", "")
//	data.Set("hFirstName", "")
//	data.Set("hMiddleName", "")
//	data.Set("hOrgName", "")
//	data.Set("hSearchOpt", "")
//	data.Set("hSearchOpt2", "")
//	data.Set("hSearchAltName", "")
//	data.Set("hSearchPartName", "")
//	data.Set("hSearchFuzzy", "")
//	data.Set("hDivision", "ALL")
//	data.Set("hBoard", "")
//	data.Set("hLicenseType", "")
//	data.Set("hSpecQual", "")
//	data.Set("hAddrType", "")
//	data.Set("hCity", "")
//	data.Set("hCounty", "")
//	data.Set("hState", "")
//	data.Set("hLicNbr", "")
//	data.Set("hAction", "")
//	data.Set("hCurrPage", "")
//	data.Set("hTotalPages", "")
//	data.Set("hTotalRecords", "")
//	data.Set("hPageAction", "")
//	data.Set("hDDChange", "")
//	data.Set("hDDChange", "Y")
//	data.Set("hBoardType", "")
//	data.Set("hLicTyp", "")
//	data.Set("hSearchHistoric", "")
//	data.Set("hRecsPerPage", "")
//	data.Set("Board", "400")
//	data.Set("City", "")
//	data.Set("County", "")
//	data.Set("State", "")
//	data.Set("RecsPerPage", "10")
//	data.Set("hSID", "")
//	data.Set("hSID", "")
//
//	resp, err := http.PostForm("https://www.myfloridalicense.com/wl11.asp?mode=1&SID=&brd=&typ=N", data)
//	if err != nil {
//		t.Fatal(err)
//	}
//	defer resp.Body.Close()
//	doc, err := goquery.NewDocumentFromReader(resp.Body)
//	categoryMap := ParseLicenseCategory(doc)
//	for name, id := range mapTestCategoryMap {
//		if categoryMap[name] != id {
//			t.Fail()
//		}
//	}
//}
//
//func TestParseSearchResults(t *testing.T) {
//	var TestResult = []SearchResult{SearchResult{Name: "JOHNSON AND BLACK NATURAL HAIR EDUCATION", Href: "LicenseDetail.asp?SID=&id=B272E08C22D51FFE2576E9E6D8EDD686"}, SearchResult{Name: "NETTA NAILS AND HAIR BOUTIQUE INC", Href: "LicenseDetail.asp?SID=&id=C861C9EA063E9CB13007663E35FA55CF"}}
//	result := ParseSearchResults(OpenQueryFile("./testfiles/SearchResult.html"))
//	//fmt.Printf("%#v",result)
//	for i, v := range TestResult {
//		if v != result[i] {
//			t.Errorf("v = %#v but result[%d]=%#v", v, i, result[i])
//		}
//	}
//}
//
func TestParseLicenseeDetails(t *testing.T) {
	//var TestLiscenseDetails = LicenseeDetails{Name: "JOHNSON AND BLACK NATURAL HAIR EDUCATION\u00a0(Primary Name)", MainAddress: "813 WEST UNIVERSITY AVENUE\n\t\tGAINESVILLE\u00a0\u00a0Florida\u00a0\u00a032601", LicenseNumber: "NPD115", Status: "Current", Expires: ""}
	resultLicense := ParseLicenseDetails(OpenQueryFile("./testfiles/LicenseeDetails2.htm"))
	fmt.Printf("%#v", resultLicense)
	//if TestLiscenseDetails.Name != resultLicense.Name {
	//	t.Error("Name error")
	//}
	//if TestLiscenseDetails.MainAddress != resultLicense.MainAddress {
	//	t.Error("MainAddress error")
	//}
	//if TestLiscenseDetails.LicenseNumber != resultLicense.LicenseNumber {
	//	t.Error("LicenseNumber error")
	//}
	//if TestLiscenseDetails.Status != resultLicense.Status {
	//	t.Error("Status error")
	//}
	//if TestLiscenseDetails.Expires != resultLicense.Expires {
	//	t.Error("Expires error")
	//}
}
func TestParseLicenseeDetailsDBAName(t *testing.T) {
	//var TestLiscenseDetails = LicenseeDetails{Name: "JOHNSON AND BLACK NATURAL HAIR EDUCATION\u00a0(Primary Name)", MainAddress: "813 WEST UNIVERSITY AVENUE\n\t\tGAINESVILLE\u00a0\u00a0Florida\u00a0\u00a032601", LicenseNumber: "NPD115", Status: "Current", Expires: ""}
	resultLicense := ParseLicenseDetails(OpenQueryFile("./testfiles/DBAname.htm"))
	fmt.Printf("name: %#v\n", resultLicense.Name)
	fmt.Printf("MainAddress: %#v\n", resultLicense.MainAddress)
	fmt.Printf("expires: %#v\n", resultLicense.Expires)
	resultLicense = ParseLicenseDetails(OpenQueryFile("./testfiles/LicenseeDetails.htm"))
	fmt.Println("LicenseeDetails.htm")
	fmt.Printf("name: %#v\n", resultLicense.Name)
	fmt.Printf("MainAddress %#v\n", resultLicense.MainAddress)
	fmt.Printf("expires: %#v\n", resultLicense.Expires)
	resultLicense = ParseLicenseDetails(OpenQueryFile("./testfiles/PrimaryName.htm"))
	fmt.Println("PrimaryName.htm")
	fmt.Printf("name: %#v\n", resultLicense.Name)
	fmt.Printf("MainAddress %#v\n", resultLicense.MainAddress)
	fmt.Printf("expires: %#v\n", resultLicense.Expires)
	resultLicense = ParseLicenseDetails(OpenQueryFile("./testfiles/LicenseeDetails2.htm"))
	fmt.Println("LicenseeDetails2.htm")
	fmt.Printf("name: %#v\n", resultLicense.Name)
	fmt.Printf("MainAddress %#v\n", resultLicense.MainAddress)
	fmt.Printf("expires: %#v\n", resultLicense.Expires)
	resultLicense = ParseLicenseDetails(OpenQueryFile("./testfiles/PalmBeach.htm"))
	fmt.Println("LicenseeDetails2.htm")
	fmt.Printf("name: %#v\n", resultLicense.Name)
	fmt.Printf("MainAddress %#v\n", resultLicense.MainAddress)
	fmt.Printf("expires: %#v\n", resultLicense.Expires)
	resultLicense = ParseLicenseDetails(OpenQueryFile("./testfiles/htmlDebugAddress.html"))
	fmt.Println("htmlDebugAddress.htm")
	fmt.Printf("name: %#v\n", resultLicense.Name)
	fmt.Printf("MainAddress %#v\n", resultLicense.MainAddress)
	fmt.Printf("expires: %#v\n", resultLicense.Expires)
	resultLicense = ParseLicenseDetails(OpenQueryFile("./testfiles/htmlDebugAddress2.html"))
	fmt.Println("htmlDebugAddress2.htm")
	fmt.Printf("name: %#v\n", resultLicense.Name)
	fmt.Printf("MainAddress %#v\n", resultLicense.MainAddress)
	fmt.Printf("expires: %#v\n", resultLicense.Expires)

}

func TestNextPage(t *testing.T) {
	//var TestLiscenseDetails = LicenseeDetails{Name: "JOHNSON AND BLACK NATURAL HAIR EDUCATION\u00a0(Primary Name)", MainAddress: "813 WEST UNIVERSITY AVENUE\n\t\tGAINESVILLE\u00a0\u00a0Florida\u00a0\u00a032601", LicenseNumber: "NPD115", Status: "Current", Expires: ""}
	result := HaveNextPage(OpenQueryFile("./testfiles/NextPage.html"))
	if result == false {
		t.Fail()
	}
	result = HaveNextPage(OpenQueryFile("./testfiles/out.html"))
	if result == true {
		t.Fail()
	}
}

func TestParseHiddenInputs(t *testing.T) {
	inputsVal := ParseHiddenInputs(OpenQueryFile("./testfiles/NextPage.html"))
	fmt.Printf("%#v \n", inputsVal)
}
func TestParsePageCount(t *testing.T) {
	inputsVal := ParsePageCount(OpenQueryFile("./testfiles/NextPage.html"))
	fmt.Printf("%#v \n", inputsVal)
}
