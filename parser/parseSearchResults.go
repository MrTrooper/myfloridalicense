package parser

import (
	"github.com/PuerkitoBio/goquery"
	"myfloridalicense"
	"net/url"
	"regexp"
	"strings"
)

func ParseSearchResults(doc *goquery.Document) (result []myfloridalicense.SearchResult, nextPage bool) {
	doc.Find(`form table tbody tr td[align="middle"] table tbody tr td table tbody tr`).Each(func(index int, item *goquery.Selection) {
		aSelection := item.Find("td font a")
		if text := aSelection.Text(); text != "" {
			if href, ok := aSelection.Attr("href"); ok {
				result = append(result, myfloridalicense.SearchResult{Name: text, Href: href})
			}
		}
	})
	nextPage = HaveNextPage(doc)
	return result, nextPage
}

func ParseCannotBeProcessed(doc *goquery.Document) bool {
	text := doc.Find(`#content-businesses > p:nth-child(1)`).Text()
	return strings.Contains(text, "sorry, but your request cannot be processed at this time")
}

func HaveNextPage(doc *goquery.Document) bool {
	//formSelect = make(map[string]string)
	SearchForwardSelector := doc.Find(`input[tabindex="5"]`)
	val, ok := SearchForwardSelector.Attr("name")
	if ok && val == "SearchForward" {
		return true
	}
	return false
}
func ParsePageCount(doc *goquery.Document) string {
	//formSelect = make(map[string]string)
	PageCount := doc.Find(`#main > tbody > tr > td > table:nth-child(1) > tbody > tr:nth-child(2) > td:nth-child(2) > form > table > tbody > tr > td > table > tbody > tr:nth-child(6) > td > table > tbody > tr:nth-child(22) > td > font`).Text()
	//val, ok := SearchForwardSelector.Attr("name")
	//if ok && val == "SearchForward" {
	//	return true
	//}
	//return false
	return PageCount
}

func ParseHiddenInputs(doc *goquery.Document) url.Values {
	data := url.Values{}
	doc.Find(`input[type="hidden"]`).Each(func(index int, item *goquery.Selection) {
		name, ok := item.Attr("name")
		if !ok {
			return
		}
		value, ok := item.Attr("value")
		if !ok {
			return
		}
		data.Set(name, value)
	})
	return data
}

func ParseLicenseDetails(doc *goquery.Document) (LD myfloridalicense.LicenseeDetails) {
	TableSelector := doc.Find(`#main > tbody  tr  td  table:nth-child(1) tbody  tr:nth-child(2)  td:nth-child(2)  table:nth-child(2) tbody > tr > td > table > tbody > tr:nth-child(2) > td > table:nth-child(3) > tbody > tr > td > table > tbody > tr > td`)

	LD.Name = TableSelector.Find(`  table:nth-child(1) > tbody > tr > td > table:nth-child(2) > tbody > tr:nth-child(1) > td:nth-child(3) > font`).Text()
	//DBAName := TableSelector.Find(` table:nth-child(1) > tbody > tr > td > table:nth-child(2) > tbody > tr:nth-child(2) > td:nth-child(3) > font`).Text()
	DBAName, _ := TableSelector.Find(` table:nth-child(1) > tbody > tr > td > table:nth-child(2) > tbody > tr:nth-child(2) > td:nth-child(3) > font`).Html()

	//
	MainAddressText := TableSelector.Find(` table:nth-child(1) > tbody > tr > td > table:nth-child(2) > tbody > tr:nth-child(8) > td:nth-child(3) > font > b`).Text()
	replaceUnicodeSymbol(&MainAddressText)
	//fmt.Println("MainAddressText", MainAddressText)
	//fmt.Println("MainAddressText", strings.TrimSpace(MainAddressText) == "")
	var MainAddress string
	if strings.TrimSpace(MainAddressText) != "" {
		MainAddress, _ = TableSelector.Find(` table:nth-child(1) > tbody > tr > td > table:nth-child(2) > tbody > tr:nth-child(8) > td:nth-child(3) > font > b`).Html()
	} else {
		MainAddress, _ = TableSelector.Find(` table:nth-child(1) > tbody > tr > td > table:nth-child(2) > tbody > tr:nth-child(3) > td:nth-child(3) > font > b`).Html()
	}

	if strings.Contains(DBAName, "(DBA Name)") {
		LD.Name = strings.TrimSpace(LD.Name) + "--NEWLINE--" + strings.TrimSpace(DBAName)
		LD.MainAddress = MainAddress
	} else {
		LD.MainAddress = DBAName
	}

	LD.LicenseNumber = TableSelector.Find(` table:nth-child(3) > tbody > tr > td > table:nth-child(2) > tbody > tr:nth-child(3) > td:nth-child(3) > font`).Text()
	LD.Status = TableSelector.Find(`table:nth-child(3) > tbody > tr > td > table:nth-child(2) > tbody > tr:nth-child(4) > td:nth-child(3) > font`).Text()
	LD.Expires = TableSelector.Find(`table:nth-child(3) > tbody > tr > td > table:nth-child(2) > tbody > tr:nth-child(6) > td:nth-child(3) > font`).Text()

	replaceUnicodeSymbol(&LD.Name)
	replaceUnicodeSymbol(&LD.MainAddress)
	replaceUnicodeSymbol(&LD.LicenseNumber)
	replaceUnicodeSymbol(&LD.Status)
	replaceUnicodeSymbol(&LD.Expires)

	return LD
}

var re = regexp.MustCompile("\\n|\\t")
var reNewLine = regexp.MustCompile("--NEWLINE--")
var reAMP = regexp.MustCompile("(\u00a0)|(&amp;)")

//var newLineRe = regexp.MustCompile("<br(|/|\\s.)>")
var newLineRe = regexp.MustCompile("<br(|/|\\s.)>")
var allOtherTextTags = regexp.MustCompile(`(<(|/)b>)|(<(|/)small>)`)

func replaceUnicodeSymbol(q *string) {
	*q = re.ReplaceAllString(*q, "")
	*q = reAMP.ReplaceAllString(*q, " ")
	//*q = strings.Replace(*q, "\u00a0", " ", -1)
	*q = strings.TrimSpace(*q)
	*q = newLineRe.ReplaceAllString(*q, "--NEWLINE--")
	*q = allOtherTextTags.ReplaceAllString(*q, "")
	if reNewLine.MatchString(*q) {
		*q = "--DOUBLE-QUOTE--" + *q + "--DOUBLE-QUOTE--"
	}
}
